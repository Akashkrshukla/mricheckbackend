from flask import jsonify,request, Flask
from flask_restful import Resource , Api
import werkzeug
from config_env import apiHost
from config_env import PORT


app = Flask(__name__)
api = Api(app)




#Convention user
#response.status refers if everything is working fine
#this will be false in case of some network error  or invalid request

#reponse.data.status refers to the operation performed 
#whether it succeeded or failed

class Home(Resource):
  def get(self):         
    return { 'message' : 'true',}



class User(Resource):
  def get(self,user_id):
    return {
      'message' :  'User registration successful'.format(user_id)
    }
  def put(self , user_id):
    return {
      'message' : 'put message received'.format(user_id)
    }



api.add_resource(Home , '/')
api.add_resource(User, '/user','/<string:user_id>')

if __name__ == '__main__':
    app.run(host="127.0.0.1", port=PORT ,debug=True)

